******************************
Documentação do projeto Robutil
******************************

.. figure:: _static/imagens/Logos/LogoRobutilFeliz.png
    :align: center
    :alt: Logo ROBUTIL
    :figclass: align-center

Olá, seja bem vindo a documentação oficial do Robutil. Aqui você encontrará todo o conteúdo para montagem e uso do robô, assim como alguns exemplos de projetos. Se você quiser colaborar, ou viu algum erro, fique a vontade para entrar em contato com os administradores do projeto.

.. image:: _static/imagens/Diversos/Robutil_MDF.png
   :width: 70%

A documentação está dividida em tópicos, onde cada tópico é um diretório com o conteúdo específico. Se tiver dificuldade para encontrar as informações, por favor deixe um feedback.

Se quiser entrar em contato com os administradores do projeto, envie um e-mail para: robutil@geniocristao.com.br

O Robutil foi criado com o intuito de ajudar professores que querem ensinar programação de forma mais atraente.

História do Nome
================

O nome foi baseado na união das palavras ROBÔ e ÚTIL, a palavra útil foi retirada do significado do nome ONÉSIMO, 
personagem bíblico da carta de Filemon, onde o seu significado do nome ONÉSIMO é ÚTIL que após “nascido de novo”, 
deixou de ser inútil para ser útil, conforme o texto abaixo:

Ele antes lhe era inútil, mas agora é útil, tanto para você quanto para mim.
Filemom 1:11

Neste caso, utilizando-se da analogia com esse livro bíblico, o Robutil é um robô que terá duas funções, 
ser um artefato pedagógico de ensino de eletrônica, programação e robótica bem como Gadget (Item decorativo para casa) 
para levar informações sobre os missionários em campo, onde o mesmo estará conectado com a internet recebendo informações 
de uma central de conteúdo. 
Como um artefato de decoração interativo, no momento que a central de conteúdo informe sobre a situação dos missionários 
ao se aproximar do Robutil, ele emitirar informações sobre a notícia.

Componentes do Robutil
======================

Componentes
-----------

 * Mobhuino/Arduino
 * LCD 16x2
 * BUZZER
 * LED AZUL alto brilho
 * LED VERMELHO alto brilho
 * LED VERDE alto brilho
 * 2 POTENCIÔMETROS 10K
 * TECLADO MEMBRANA 4x3
 * CABO USB
 * LDR 5MM
 * RESISTORES
 * IRF3415
 * Fonte 12V5A

Diagrama
----------

.. image:: _static/imagens/Diversos/DiagramaComponentesRobutil.png
   :width: 70%

Montando o seu Robutil
=====================

.. toctree::
   :maxdepth: 2
   
   Lista de Materiais <MR_ListaMateriais.rst>
   Identificando os Componentes <MR_IdentificandoComponentes.rst>
   Montagem fisica <MR_Montagem.rst>
   Esquematico e Pinagem <MR_EsquematicoPinagem.rst>
   

Testando os Componetes do Ralabô
================================

.. toctree::
   :maxdepth: 2   
   
   BUZZER <TR_BUZZER.rst>
   BOTÃO <TR_BOTAO.rst>
   LDR <TR_LDR.rst>
   LCD <TR_LCD.rst>
   LED <TR_LED.rst>
   MOTOR <TR_MOTOR.rst>
   POTENCIOMETRO <TR_POTENCIOMETRO.rst>
   SERVO MOTOR <TR_SERVO.rst>
   ULTRASOM <TR_ULTRASOM.rst>
   

Dicas e Truques
===============

.. toctree::
   :maxdepth: 2   
   
   Utilizando o DB4K <DT_UtilizandoDB4K.rst>   
   

Exemplos de Projetos
====================

.. toctree::
   :maxdepth: 2
   
   Sistema semaforico <EX_Semaforo.rst>
