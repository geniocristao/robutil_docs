Exemplo - Sensor de luz com LDR
===============================

Nesse exemplo vamos ler um sensor LDR usando a entrada analógica. Será exibido um exemplo de funcionamento de uma fotocélula, onde ao escurecer será aceso o LED, como acontece na luz da rua.


Circuitos Envolvidos
--------------------

.. image:: _static/imagens/Circuitos/01-RalaboLDR_bb.png
   :width: 100%
   
.. image:: _static/imagens/Circuitos/02-RalaboLeds_bb.png
   :width: 100%
   
   
Pinagem
-------

Conexões no Ralabô

.. csv-table::
   :header: "COMPONENTE","DESCRIÇÃO","PINO ARDUINO"
   :widths: 10, 10, 10   
   
    "LED","VERMELHO",9
    "LDR"," ","A0"


Sketch
------

.. sourcecode:: c

    /*
    Ralabô
    Exemplo: Sensor de Luz - LDR

    Nesse exemplo vamos ler um sensor LDR usando a entrada analógica. Será exibido um exemplo de funcionamento de uma fotocélula, onde ao escurecer será aceso o LED, como acontece na luz da rua.
    */

    int ledPin   = 9;   //Led no pino 1 - LED da placa
    int ldrPin   = A0;  //LDR no pino analógico A1
    int ldrValor = 0;   //Valor lido do LDR

    void setup() {
        
        pinMode(ledPin,OUTPUT); //define LED como Saída
    
    }

    void loop() {
        
        //ler o valor do LDR
        //O valor lido será entre 0 e 1023
        ldrValor = analogRead(ldrPin);       

        //se o valor lido for maior que 800, liga o led
        if (ldrValor>= 800){
        
            digitalWrite(ledPin, HIGH);      //aciona saída
            
        }else {            
    
            digitalWrite(ledPin,LOW);        //desliga saída
        }

    delay(100);
    
    }


