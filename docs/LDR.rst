SENSOR LDR
==========

Os LDRs (Light Dependent Resistors) são componentes sensíveis à luz, ou seja, dispositivos eletrônicos que podem agir sobre um circuito em função da luz incidente numa superfície sensível dos mesmos.

.. image:: _static/imagens/Circuitos/RalaboLDR_bb.png
   :width: 100%
